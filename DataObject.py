from typing import Dict, Tuple


class XYZGroup:
    xGroup: int = 0
    yGroup: int = 0
    zGroup: int = 0

    def __init__(self, xGroup:int = 0, yGroup:int = 0, zGroup:int = 0) -> None:
        self.xGroup = xGroup
        self.yGroup = yGroup
        self.zGroup = zGroup

    def __eq__(self, __value: object) -> bool:
        if isinstance(__value, XYZGroup):
            return self.xGroup == __value.xGroup and self.yGroup == __value.yGroup and self.zGroup == __value.zGroup
        return False

class Point3D:
    x: float
    y: float
    z: float

    def __init__(self, xGroup:float = 0.0, yGroup:float = 0.0, zGroup:float = 0.0) -> None:
        self.xGroup = xGroup
        self.yGroup = yGroup
        self.zGroup = zGroup

    def __eq__(self, __value: object) -> bool:
        if isinstance(__value, Point3D):
            return self.x == __value.x and self.y == __value.y and self.z == __value.z
        return False

class DataObject:
    minX: float = 0
    minY: float = 0
    maxX: float = 0
    maxY: float = 0
    zOffset: float = 0
    sizeX: int = 0
    sizeY: int = 0
    distX: float = 0
    distY: float = 0
    
    offset: Point3D = Point3D()
    points: Dict[XYZGroup, Point3D] = {}
    _valid: bool = False
