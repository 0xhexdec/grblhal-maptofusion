#Author-Autodesk Inc.
#Description-Import spline from csv file

import adsk.core, adsk.fusion, traceback
import math

from .exceptions import FileFormatException
from .mapParser import MapParser
from . import fusion360utils as futil

app = adsk.core.Application.cast(None)
ui  = adsk.core.UserInterface.cast(None)
_handlers = []
_mapParser: MapParser

def run(context):
    try:
        global app, ui, _mapParser
        app = adsk.core.Application.get()
        ui  = app.userInterface
        # Get all components in the active design.
        cmdDef = ui.commandDefinitions.itemById('HeightmapImportScript')
        title = 'Heightmap Importer'
        if not cmdDef:
            # Create a command definition.
            cmdDef = ui.commandDefinitions.addButtonDefinition('HeightmapImportScript', title, 'Importes grblHAL map file to fusion') 
        _mapParser = MapParser()
        onCommandCreated = MapImportCommandCreatedHandler()
        cmdDef.commandCreated.add(onCommandCreated)
        _handlers.append(onCommandCreated)
        cmdDef.execute()        
        adsk.autoTerminate(False)
    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))

class MapImportCommandCreatedHandler(adsk.core.CommandCreatedEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        try:
            eventArgs = adsk.core.CommandCreatedEventArgs.cast(args)
            
            # Verify that a Fusion design is active.
            des = adsk.fusion.Design.cast(app.activeProduct)
            if not des:
                ui.messageBox('A Fusion design must be active when invoking this command.')
                return()

            cmd = eventArgs.command
            cmd.isExecutedWhenPreEmpted = False
            inputs = cmd.commandInputs
            
            inputs.addBoolValueInput('fileButton', 'Select File', False, './resources/Folder', False)

            typeDropdown = inputs.addDropDownCommandInput('typeDropdown', 'Type', adsk.core.DropDownStyles.TextListDropDownStyle)
            typeDropdown.listItems.add('Line', True)
            typeDropdown.listItems.add('Surface', False)
            typeDropdown.isEnabled = False

            # Line Options
            lineTypeDropdown = inputs.addDropDownCommandInput('lineTypeDropdown', 'Linetype', adsk.core.DropDownStyles.TextListDropDownStyle)
            lineTypeDropdown.listItems.add('Line', True)
            lineTypeDropdown.listItems.add('Spline', False)
            lineTypeDropdown.isEnabled = False
            
            # Connect to the command related events.
            onExecute = MapImportCommandExecuteHandler()
            cmd.execute.add(onExecute)
            _handlers.append(onExecute)        
            
            onInputChanged = MapImportCommandInputChangedHandler()
            cmd.inputChanged.add(onInputChanged)
            _handlers.append(onInputChanged)     
            
            onValidateInputs = MapImportCommandValidateInputsHandler()
            cmd.validateInputs.add(onValidateInputs)
            _handlers.append(onValidateInputs)

            onDestroy = MapImportCommandDestroyHandler()
            cmd.destroy.add(onDestroy)
            _handlers.append(onDestroy)
        except:
            if ui:
                ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))

class MapImportCommandExecuteHandler(adsk.core.CommandEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        product = app.activeProduct
        design = adsk.fusion.Design.cast(product)
        root = design.rootComponent
        sketch = root.sketches.add(root.xYConstructionPlane)
        fusionPoints = adsk.core.ObjectCollection.create()
        for x in range(0, _mapParser.sizeX):
            for y in range(0, _mapParser.sizeY):
                fusionPoints.add(adsk.core.Point3D.create(x*_mapParser.distX, y*_mapParser.distY, _mapParser.points[(x,y)]))
            sketch.sketchCurves.sketchFittedSplines.add(fusionPoints)
            fusionPoints.clear()


class MapImportCommandInputChangedHandler(adsk.core.InputChangedEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        changed_input = args.input
        inputs = args.inputs

        if changed_input.id == 'fileButton':
            fileDialog = ui.createFileDialog()
            fileDialog.title = 'Open .map File'
            fileDialog.filter = 'map file (*.map);;All Files (*.*)'
            if fileDialog.showOpen() != adsk.core.DialogResults.DialogOK :
                return
            try:
                _mapParser.readFile(fileDialog.filename)
            except FileFormatException as e:
                futil.log(e.message, adsk.core.LogLevels.ErrorLogLevel)

class MapImportCommandValidateInputsHandler(adsk.core.ValidateInputsEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        if _mapParser.holdsValidData():
            args.areInputsValid = True
        else: 
            args.areInputsValid = False

class MapImportCommandDestroyHandler(adsk.core.CommandEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        try:
            eventArgs = adsk.core.CommandEventArgs.cast(args)

            # when the command is done, terminate the script
            # this will release all globals which will remove all event handlers
            adsk.terminate()
        except:
            if ui:
                ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))


