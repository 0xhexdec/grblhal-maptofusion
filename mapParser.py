from typing import Dict, Tuple
import adsk.core, adsk.fusion, traceback
import io
import xml.etree.ElementTree as ElementTree

from .exceptions import FileFormatException
from . import fusion360utils as futil

class MapParser:
    minX: float = 0
    minY: float = 0
    maxX: float = 0
    maxY: float = 0
    zOffset: float = 0
    sizeX: int = 0
    sizeY: int = 0
    distX: float = 0
    distY: float = 0
    points: Dict[Tuple[int, int],float] = {}
    _valid: bool = False
    
    def readFile(self, filepath: str):
        app = adsk.core.Application.get()
        ui  = app.userInterface
        # Get all components in the active design.
        product = app.activeProduct
        design = adsk.fusion.Design.cast(product)
        title = 'Map file Importer'
        with io.open(filepath, 'r', encoding='utf-8-sig') as f:
            root = ElementTree.fromstringlist(f.readlines())
            if root.tag != 'heightmap':
                raise FileFormatException("File does not contain a <heightmap> tag that should be present in a .map file.")
            try:
                self.minX = int(root.attrib["MinX"])
                self.minY = float(root.attrib["MinY"])
                self.maxX = float(root.attrib["MaxX"])
                self.maxY = float(root.attrib["MaxY"])
                self.sizeX = int(root.attrib["SizeX"])
                self.sizeY = int(root.attrib["SizeY"])
                self.distX = (self.maxX - self.minX) / (self.sizeX - 1)
                self.distY = (self.maxY - self.minY) / (self.sizeY - 1)
                futil.log(self.distX)
                futil.log(self.distY)

                for child in root:
                    self.points[(int(child.attrib["X"]), int(child.attrib["Y"]))] = float(child.text)
                self._valid = True
                futil.log(self.points)
            except Exception as e:
                raise FileFormatException(e) 
    
    def holdsValidData(self) -> bool:
        return self._valid